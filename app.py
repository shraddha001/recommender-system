import streamlit as st
import pickle
import pandas as pd

RL_output_data = pickle.load(open('RL_output_data2.pkl','rb'))
RL_input_fetch_data = pickle.load(open('RL_input_fetch_data2.pkl','rb'))
def top_five_high(product_id):
     match_id = list(RL_output_data.loc[product_id].sort_values(ascending=False)[:5].index)
     return match_id

st.title("Reinforcement Learning Recommender system")
#st.write(streamlit --version,python --version)

selected_movie_name = st.selectbox(
     'Select Item',
     (RL_input_fetch_data['pro'].values))



if st.button('Recommend'):
     match_id = top_five_high(selected_movie_name)
     #st.write("working")

     col1, col2, col3, col4, col5 = st.columns(5)

     with col1:
          st.write("product Name: ", RL_input_fetch_data[RL_input_fetch_data['pro'] == match_id[0]]['pro'].values[0])
          if RL_input_fetch_data[RL_input_fetch_data['pro'] == match_id[0]]['pro'].values[0] =="clocks 12":
               st.image("https://images-na.ssl-images-amazon.com/images/I/812L5zyAmpL.__AC_SX300_SY300_QL70_FMwebp_.jpg")
          else:
               st.image("https://static.streamlit.io/examples/owl.jpg")


          st.write("product Details: ",
                   RL_input_fetch_data[RL_input_fetch_data['pro'] == match_id[0]]['product'].values[0])

     with col2:
          #st.header("A dog")
          st.write("product Name: ", RL_input_fetch_data[RL_input_fetch_data['pro'] == match_id[1]]['pro'].values[0])
          if RL_input_fetch_data[RL_input_fetch_data['pro'] == match_id[1]]['pro'].values[0] =="clocks 411":
               st.image("https://img.muji.net/img/item/4549337330047_1260.jpg")
          else:
               st.image("https://static.streamlit.io/examples/owl.jpg")
          st.write("product Details: ",
                   RL_input_fetch_data[RL_input_fetch_data['pro'] == match_id[1]]['product'].values[0])

     with col3:
          #st.header("An owl")
          st.write("product Name: ", RL_input_fetch_data[RL_input_fetch_data['pro'] == match_id[2]]['pro'].values[0])
          if RL_input_fetch_data[RL_input_fetch_data['pro'] == match_id[2]]['pro'].values[0] =="clocks 397":
               st.image("https://rukminim1.flixcart.com/image/416/416/k0wqwsw0/wall-clock/v/m/h/cola-3001a-cola-analog-precisio-original-imafkhkgagsgwyxj.jpeg?q=70")
          else:
               st.image("https://static.streamlit.io/examples/owl.jpg")
          st.write("product Details: ",
                   RL_input_fetch_data[RL_input_fetch_data['pro'] == match_id[2]]['product'].values[0])

     with col4:
          #st.header("A dog")
          st.write("product Name: ", RL_input_fetch_data[RL_input_fetch_data['pro'] == match_id[3]]['pro'].values[0])
          if RL_input_fetch_data[RL_input_fetch_data['pro'] == match_id[3]]['pro'].values[0] =="tablet 50":
               st.image("https://rukminim1.flixcart.com/image/416/416/jyeq64w0/wall-clock/t/x/q/wall-clock-w0001pa01-analog-titan-original-imafgnyzufgmvafk.jpeg?q=70")
          else:
               st.image("https://static.streamlit.io/examples/owl.jpg")
          st.write("product Details: ", RL_input_fetch_data[RL_input_fetch_data['pro'] == match_id[3]]['product'].values[0])

     with col5:
          #st.header("An owl")
          st.write("product Name: ", RL_input_fetch_data[RL_input_fetch_data['pro'] == match_id[4]]['pro'].values[0])
          if RL_input_fetch_data[RL_input_fetch_data['pro'] == match_id[4]]['pro'].values[0] =="clocks 169":
               st.image("https://res.cloudinary.com/rsc/image/upload/b_rgb:FFFFFF,c_pad,dpr_1.0,f_auto,q_auto,w_700/c_pad,w_700/F6699252-01")
          else:
               st.image("https://static.streamlit.io/examples/owl.jpg")
          st.write("product Details: ", RL_input_fetch_data[RL_input_fetch_data['pro'] == match_id[4]]['product'].values[0])