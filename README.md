Recommender System – RL
 recommender top five items
 
File Description
1.	recommends rl-Copy3.ipynb
2.	file1.csv
3.	RL_output_data2.pkl
4.	RL_input_fetch_data2.pkl
5.	app.py
6.	setup.sh
7.	.gitignore
8.	Procfile
9.	requirements.txt
Explanation
The aim of this project is to make a recommendation system with Reinforcement Learning. firstly, get the dataset. performed some pre-processing and feature engineering tasks on the dataset. now we had a good dataset. now we are using CountVectorizer() for converting text into a vector. we can find the top five similar products. product name column helps us to make Q-learning input data. we are putting data on Q-learning as input data. as a result, we find the top five values in each row. this list of trees will convert a dataset. we will create two new datasets using the pickle library. these two datasets move on PyCharm IDE. On PyCharm IDE we are using Streamlit. Streamlit is an open-source app framework for Machine Learning. here we are using a button and drop-down for the product name list. when you click on the button you will get the top five similar products at the bottom.
Dataset
1.	file1.csv
2.	RL_output_data2.pkl
3.	RL_input_fetch_data2.pkl





